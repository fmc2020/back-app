import { ObjRoute } from '../../../utils/routes';
import * as Services from 'services';

export const routes = [
  new ObjRoute('get', '/user/tags/search/:search',  Services.Tags.search),
  new ObjRoute('post', '/api/tags/new',  Services.Tags.newTag),
];
