import { ObjRoute } from '../../../utils/routes';
import * as Services from 'services';

export const routes = [
  new ObjRoute('get', '/user/rooms/list',  Services.Rooms.List),
];
