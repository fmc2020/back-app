import * as User from './user';
import * as Tags from './tags';
import * as Rooms from './rooms';

export const routes = [
  ...User.routes,
  ...Tags.routes,
  ...Rooms.routes,
];
