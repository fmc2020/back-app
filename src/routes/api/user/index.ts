import { ObjRoute } from '../../../utils/routes';
import * as Services from 'services';

export const routes = [
  new ObjRoute('post', '/api/user/check_user',  Services.User.Auth.checkUser),
  new ObjRoute('post', '/api/user/auth',  Services.User.Auth.authUser),
  new ObjRoute('post', '/user/about',  Services.User.Auth.update),
  new ObjRoute('post', '/user/aboutTags',  Services.User.Auth.updateTags),
  new ObjRoute('post', '/api/user/reg',  Services.User.Auth.regUser),
  new ObjRoute('post', '/api/user/reg_sms',  Services.User.Auth.regSms),
];
