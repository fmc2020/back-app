import * as Express from 'express';
import * as SerializeError from 'serialize-error';

const responseWithStatus = (endpoint: string, payload: any, error: any, status = 200) => {
  const answer = JSON.stringify(payload);
  return answer;
};

export const handleRequest = (response: Express.Response, data: any, status: number = 200) => {
  if (data.then && data.constructor.name === 'Promise') {
    data.then((result: any) => {
      response.status(status).end(responseWithStatus(response.req.url, result, null, status));
    }).catch(async (error: Error) => {
      const serialError = SerializeError(error);
      const status = serialError.status || 500;
      response.status(status as number).end(
        responseWithStatus(response.req.url,
                           null, {
                             message: serialError.message,
                           },
                           status as number));
    });
  } else {
    response.status(status).end(responseWithStatus(response.req.url, data, null, status));
  }
};
