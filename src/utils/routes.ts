import { RequestWithContext } from 'types';
import * as Express from 'express';
import { handleRequest } from './handleRequest';

export class ObjRoute {
  public m: any;
  public p: any;
  public f: any;

  constructor(m: any, p: any, f: any) {
      this.m = m;
      this.p = p;
      this.f = f;
    }

  get() {
      return  {
          method: this.m,
          path: this.p,
          f: (request: RequestWithContext, response: Express.Response) =>
                handleRequest(response, (new this.f(request)).exec(), 200),
        };
    }
}
