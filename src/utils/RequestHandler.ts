import {Err, RequestWithContext} from 'types';
import {IncomingHttpHeaders} from 'http';
import {Logger} from "utils";

export class RequestHandler {
    public body: any;
    public headers: IncomingHttpHeaders;
    public ctx: any;
    public params: any;

    constructor(request: RequestWithContext) {
        this.body = request.body;
        this.headers = request.headers;
        this.ctx = request.context;
        this.params = request.params;
    }

    async exec() {
        try {
            return await this.run()
        } catch (e) {
            await Logger.send('uncaught_error', {msg: e.message, stack: e.stack});
            return Err('Техническая ошибка')
        }
    }

    async run() {

    }
}
