import { Noti } from 'modules';
import {CErrors} from "../components/errors";
import * as Moment from 'moment';

export const Logger = new class {

    constructor() {
    }

    async send(type: String, data: {msg: String, stack: String}) {
        let error = await CErrors.models.Errors.create({
            msg: data.msg,
            stack: data.stack,
            date_create: Moment().toISOString()
        })
        return  Noti.sendLog(type, {id: error._id, msg: data.msg.slice(0,1023)});
    }
}
