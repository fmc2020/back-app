import * as Url from 'url';
import * as Utils from 'utils';
import * as Services from 'services';
import {RequestHandler, Response} from 'express';
import {RequestWithContext} from 'types';
import {Sequelize} from 'sequelize';
import {CUser} from "components";

export const access: RequestHandler =
    async (request: RequestWithContext, response: Response, next: Function) => {
        request.context = {};
        request.context.ua = request.headers['user-agent'];
        request.context.app = 'findUCompany-web';
        request.context.token = request.headers['user_token'];

        const parameters = Url.parse(request.url);
        const accessDeniedMessage = 'Access denied.';
        if (parameters.path.indexOf('/user/') === 0) {

            if (!request.context.token) {
                return Utils.handleRequest(response, accessDeniedMessage, 401);
            }

            const dbUser = await CUser.checkToken(request.context);

            if (!dbUser) {
                return Utils.handleRequest(response, accessDeniedMessage, 401);
            }
            request.context.User = dbUser;
        }
        return next();
    };
