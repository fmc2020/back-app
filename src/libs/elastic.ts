const { Client } = require('@elastic/elasticsearch');

export const Elastic = class {
    public Elastic: any;
    protected index: string;
    private readonly analyzer: string;
    protected fields: string[];

    constructor() {
        this.index = '';
        this.Elastic = new Client({ node: 'http://localhost:9200' });
        this.analyzer = 'latin';
        this.fields = [];
    }
    async insert(data: any) {
        let query = [];
        let params = {index: {}};
        if (data._id) {
            // @ts-ignore
            params.index._id = data._id;
            delete data._id;
        }
        query.push(params);
        query.push(data);
        const {body: bulkResponse} = await this.Elastic.bulk({index: this.index, body: query})
            .catch((err: any) => console.error('err', JSON.stringify(err)));
        if (bulkResponse.errors) {
            console.error(JSON.stringify(bulkResponse.items))
        }
    }

    async exec(ids: String[], script: String, params: any) {
        if (!ids || !ids.length)
            throw new SyntaxError("ids is required");

        return this.Elastic.updateByQuery({
            index: this.index,
            body: {
                query: {
                    terms: {
                        _id: ids,
                    }
                },
                script: {
                    source: script,
                    lang: "painless",
                    params,
                }
            }
        })
    }

    async search(search: any, city: String) {

        if(!search)
            throw new SyntaxError("str is required");

        search = search.replace(/[^\s\da-zа-яё]/gi,'')
            .split(/\s+/g)
            .filter(Boolean)
            .join('~ ') + '~';

        let query =
            {
                query: {
                    query_string: {
                        fields: this.fields,
                        query: search,
                        analyzer: this.analyzer,
                        fuzziness: 3,
                        fuzzy_max_expansions: 10,
                    }
                },
                aggs : {
                    id: {
                        terms: {
                            field: "_id",
                            size: 10,
                            order: {
                                users_count: "desc"
                            }
                        },
                        aggs: {
                            users_count: {
                                value_count: {
                                    field: `users.${city}.keyword`
                                }
                            }
                        }
                    }
                }
            };

        let {body: resp} = await this.Elastic.search({index: this.index, body: query})
            .catch((err: any) => console.error('err', JSON.stringify(err)));


        return {hits: resp.hits.hits, aggs: resp.aggregations};

    }
};
