import * as Mongodb from 'mongoose';

// подключение
Mongodb.connect('mongodb://localhost:27017/core', { useNewUrlParser: true, useUnifiedTopology: true });

export {
    Mongodb,
};
