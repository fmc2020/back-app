import * as FS from 'fs';

export const config = {
  express: {
    port: process.env['express.port'],
    ioPort: process.env['express.ioPort'],
  },
  rabbit: {
    connectionURL: 'amqp://zz@185.144.29.188//',
  },
};
