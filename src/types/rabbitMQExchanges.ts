export enum RabbitMQExchanges {
  Direct = 'amq.direct',
  Public = 'amq.exchange',
}
