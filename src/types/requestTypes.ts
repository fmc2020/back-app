export const Su = (data?: any) => ({
  type: 'su',
  data,
});

export const Err = (msg: String) => ({
  type: 'err',
  msg,
});
