import { RequestHandler } from 'utils';
import { Err, Su } from 'types';
import * as Bcrypt from 'bcrypt';
import { CUser } from 'components';

export class regUser extends RequestHandler {

  async run() : Promise<any> {
      const login = this.body.login.replace(/[ \(\)-]/g, '');
      const user = await CUser.models.User.findOne({ phone: login });

      if (user) {
          return Err('Пользователь с таким номером существует');
        }

      const hash = Bcrypt.hashSync(this.body.password, 10);

      const newUsers = await CUser.models.User.insertMany({
          phone: login,
          hash,
          age: this.body.age,
          name: this.body.name,
        });
      const newUser = newUsers.pop();

      const session = await CUser.makeSession(newUser.id, this.ctx.ua, this.ctx.app);
      return Su({ user: newUser.getForUser(), session: session.getForUser() });
    }
}
