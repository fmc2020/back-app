import { RequestHandler } from 'utils';
import * as Bcrypt from 'bcrypt';
import { Err, Su } from 'types';
import { CUser } from 'components';
import { Noti } from 'modules';

export class checkUser extends RequestHandler {

  async run(): Promise<any> {
      const login = this.body.login.replace(/[ \(\)-]/g, '');
      const user = await CUser.models.User.findOne({ phone: login });

      if (!user) {
          await CUser.newRegistration(login);
        }

      return Su(!!user);

    }
}
