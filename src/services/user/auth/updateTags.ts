import {RequestHandler} from 'utils';
import {Err, Su} from 'types';
import {CTags, CUser} from 'components';

export class updateTags extends RequestHandler {

    async run(): Promise<any> {
        let data = this.body;

        if (!data.chosen_tags.length)
            return Err('Выберите хотя бы один тег.');

        await CTags.models.Tags.updateTagsUser(data.chosen_tags.map((tag: any) => tag._id),
            this.ctx.User.city, [this.ctx.User._id]);

        let user = await CUser.models.User.findByIdAndUpdate(this.ctx.User._id, {
            tags: data.chosen_tags.map((tag: any) => tag._id)
        }, {new: true});

        return Su(user.getForUser());


    }
}
