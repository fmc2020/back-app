import { RequestHandler } from 'utils';
import * as Bcrypt from 'bcrypt';
import { Err, Su } from 'types';
import {CUser} from 'components';

export class authUser extends RequestHandler {

  async run() : Promise<any> {
      const login = this.body.login.replace(/[ \(\)-]/g, '');
      const user = await CUser.models.User.findOne({ phone: login });
      if (!user) {
          return Err('Пользователь не найден');
        }

        // @ts-ignore
      const isRightPwd = await Bcrypt.compare(this.body.password, user.hash);

      if (!isRightPwd) {
          return Err('Неверный пароль');
        }

      const session = await CUser.makeSession(user.id, this.ctx.ua, this.ctx.app);
      return Su({ user: user.getForUser(), session: session.getForUser() });

    }
}
