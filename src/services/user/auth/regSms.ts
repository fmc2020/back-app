import { RequestHandler } from 'utils';
import { Err, Su } from 'types';
import {CUser} from "components";

export class regSms extends RequestHandler {

    async run() : Promise<any> {
        const login = this.body.login.replace(/[ \(\)-]/g, '');
        const authorize = await CUser.models.Registration.findOne({ phone: login, code: this.body.sms});

        if (!authorize) {
            return Err('Неверный код подтверждения');
        } else {
            await CUser.models.Registration.deleteOne({_id: authorize._id});
            return Su();
        }
    }
}
