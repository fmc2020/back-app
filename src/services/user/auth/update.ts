import {RequestHandler} from 'utils';
import {Err, Su} from 'types';
import {CTags, CUser} from 'components';

export class update extends RequestHandler {

    async run(): Promise<any> {
        let data = this.body;
        if (!data.name || !data.gender) {
            return Err('Заполните информацию о себе');
        }
        let user = await CUser.models.User.findByIdAndUpdate(this.ctx.User._id, {
            name: data.name,
            gender: data.gender,
            age: data.age,
            city: data.city,
        }, {new: true});

        return Su(user.getForUser());

    }
}
