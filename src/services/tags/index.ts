import {RequestHandler} from 'utils';
import {Err, Su} from 'types';
import {CTags} from "components";

export class newTag extends RequestHandler {

  async run(): Promise<any> {
    await CTags.models.Tags.insert(this.body.tag);
    return Su();
  }
}

export class search extends RequestHandler {

  async run(): Promise<any> {
    let result = await CTags.models.Tags.search(this.params.search, this.ctx.User.city);
    let hits = result.hits;

    let answer = result.aggs.id.buckets.map((back: any) => ({
      _id: back.key,
      ...result.hits.find((hit: any) => hit._id === back.key)._source,
      users_count: back.users_count.value,
    }));

    return Su(answer);
  }
}
