import {Tags} from "./models/Tags";

export const CTags = new class {
    public models: any;

    constructor() {
        this.models = {
            Tags,
        }
    };
};
