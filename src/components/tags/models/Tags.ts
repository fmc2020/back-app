import {Elastic} from "libs";

export const Tags = new class extends Elastic {
    index: string;
    constructor() {
        super();
        this.index = 'tags';
        this.fields = [ "name", "desc" ];
    }

    updateTagsUser(ids: String[], city: String, users: String[],) {
        let script = `
        if (ctx._source.get('users') == null) 
            ctx._source.users = new HashMap();
        if (ctx._source.users.get('${city}') == null) 
            ctx._source.users['${city}'] = new ArrayList(); 
        ctx._source.users['${city}'].addAll(params.data)
        `;

        script = script.replace(/\n|\s\s/g, '');

        let params = {
            data: users
        };

        return this.exec(ids, script, params);
    }
};
