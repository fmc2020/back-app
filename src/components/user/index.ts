import * as Moment from 'moment';
import { Noti } from 'modules';
import {Session} from "./models/Session";
import {Registration} from "./models/Registration";
import {User} from "./models/User";

const crypto = require('crypto');

export const CUser = new class{
    public models: any;

    constructor() {
        this.models = {
            Session,
            Registration,
            User,
        }
    }

    _genToken = () => {
        return crypto.randomBytes(24)
            .toString('base64')
            .replace(/\W/g, '');
    };

    checkToken = async (ctx: any) => {
        let session = await this.models.Session.findOne({token: ctx.token}) as any;

        if (!session) {
            throw 'no session';
        }

        if (Moment(session.next_update) < Moment()) {
            await this.updateSession(ctx.token, ctx.ua, ctx.app);
        }

        return this.getUserByID(session.user_id);
    };

    makeSmsCode = () => {
        return crypto.randomBytes(24).map((a: any) => a%10).join('').slice(0, 6)
    };

    getUserByID = (id: any) => this.models.User.findOne({_id: id});

    makeSession = async (id: String, device: String, app: String) => {
        await this.models.Session.deleteMany({ user_id: id });

        const sessions = await this.models.Session.insertMany({
            token: this._genToken(),
            app,
            user_id: id,
            date_create: Moment().toISOString(),
            date_update: Moment().toISOString(),
            next_update: Moment().add(7, 'd').toISOString(),
            last_device: device,
        });
        return sessions[0];
    };

    updateSession = (token: String, device: String, app: String) => {
        return this.models.Session.updateOne({ token, last_device: device, app }, {
            date_update: Moment().toISOString(),
            next_update: Moment().add(7, 'd').toISOString(),
        }, {new: true});
    };

    newRegistration = async (login: String) => {

        const code = this.makeSmsCode();

        await this.models.Registration.deleteMany({phone: login});

        await this.models.Registration.insertMany({
            phone: login,
            code,
            date_create: Moment().toISOString(),
            authorized: false,
        });
        await Noti.sendSmsMessage(login, `Ваш код подтверждения: ${code}`);
    };
};
