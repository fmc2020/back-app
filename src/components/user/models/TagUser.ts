import {Elastic} from "libs";

export const TagUser = new class extends Elastic {
    index: string;
    constructor() {
        super();
        this.index = 'user_tags';
    }
};
