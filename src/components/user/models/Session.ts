import * as Mongodb from "mongoose";
import {Schema} from "mongoose";

const sessionScheme = new Schema({
    token: String,
    app: String,
    user_id: String,
    date_create: String,
    date_update: String,
    next_update: String,
    last_device: String,
});

sessionScheme.methods.getForUser = function () {
    const model: any = this;
    return {
        token: model.token,
        next_update: model.next_update,
    };
};

export const Session = Mongodb.model<any>('Session', sessionScheme);
