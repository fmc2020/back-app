import * as Mongodb from 'mongoose';

const Schema = Mongodb.Schema;

const userScheme = new Schema<any>({
    name: String,
    age: Number,
    phone: String,
    gender: Number,
    hash: String,
    city: String,
    tags: Array,
});

userScheme.methods.getForUser = function () {
    const model: any = this;
    return {
        name: model.name,
        age: model.age,
        phone: model.phone,
        gender: model.gender,
        city: model.city,
        tags: model.tags,
    };
};

export const User = Mongodb.model<any>('User', userScheme);
