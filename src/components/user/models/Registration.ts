import * as Mongodb from "mongoose";
import {Schema} from "mongoose";

const regScheme = new Schema({
    phone: String,
    code: String,
    date_create: String,
    authorized: Boolean,
});

export const Registration = Mongodb.model<any>('Registrations', regScheme);
