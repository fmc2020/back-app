import * as Mongodb from "mongoose";
import {Schema} from "mongoose";

const regScheme = new Schema({
    msg: String,
    stack: String,
    date_create: String,
});

export const Errors = Mongodb.model<any>('Errors', regScheme);
