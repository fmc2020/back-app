import {Errors} from "./models/Errors";

export const CErrors = new class {
    public models: any;

    constructor() {
        this.models = {
            Errors,
        }
    };
};
