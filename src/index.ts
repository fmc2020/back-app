import * as Aliases from 'module-alias';
import * as DotEnv from 'dotenv';

Aliases.addAliases({
  utils: `${__dirname}/utils`,
  config: `${__dirname}/config.ts`,
  types: `${__dirname}/types`,
  models: `${__dirname}/db/models`,
  handlers: `${__dirname}/db/handlers`,
  services: `${__dirname}/services`,
  components: `${__dirname}/components`,
  modules: `${__dirname}/modules`,
  libs: `${__dirname}/libs`,
  sharedConstants: `${__dirname}/constants`,
});

DotEnv.config({
  path: process.env.NODE_ENV === 'development' ? '.env-development' : '.env',
});

require('./application').initialize();
